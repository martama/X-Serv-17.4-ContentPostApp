#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


class contentApp (webapp.webApp): #heredo de WebApp
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""

        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\n')[-1]

        return method, resource, body

    def process(self, resourceandmethod):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        (method, resource, body) = resourceandmethod

        formulario = """<form action=" " method="POST">
          Introduce el contenido del recurso: <input name="something" type="text" />
           <input type="submit" value="Submit" />
            </form>"""

        if method == "GET":
            if resource in self.content:
                httpCode = "200 OK"
                htmlBody = "<html><body>" + self.content[resource]
            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body> Not Found"

        elif method == "POST":
            body_content = body.split("=")[1]
            self.content.update({resource : body_content})
            print("El diccionario actualizado es: \n", self.content)
            htmlBody = "<html><body>"
            httpCode = "200 ok"
            pass

        htmlBody += formulario + "</body></html>"


        return (httpCode, htmlBody)




if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
